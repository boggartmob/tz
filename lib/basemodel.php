<?php
/**
 * Created by Pavel Vorobev.
 * Date: 17.12.14
 * Time: 20:35
 */

namespace lib;


class BaseModel {
	protected $tableAlias = 't';
	protected static $tableName = 'fake_table';
	protected $exists = false;
	
	public $id;
	public $errors = [];

	public function delete()
	{
		$class = get_class($this);
		$tableName = $class::$tableName;

		$query = 'DELETE FROM `' . $tableName .'` WHERE `' . $tableName .'`.`id`=?';
		DBConnection::query($query, [$this->id]);
		$this->exists = false;

		foreach($this->getFields() as $field) {
			$this->{$field} = null;
		}

		DBConnection::free();
	}

	protected function getFields() {
		$inheritedFields = array_keys(get_class_vars(__CLASS__));
		$modelFields = array_keys(get_class_vars(get_class($this)));
		$dbFields = array_diff($modelFields, $inheritedFields);

		return $dbFields;
	}
	
	protected function afterFind() {
		//-- override
	}

	/**
	 * @param $data
	 */
	protected function fromData($data) {
		$this->exists = !is_null($data);

		if (!$this->exists) {
			return;
		}

		$this->id = $data['id'];

		foreach($this->getFields() as $field) {
			$this->{$field} = $data[$field];
		}
		
		$this->afterFind();
	}
	
	public function hasErrors() {
		return sizeof($this->errors) > 0;
	}
	
	public function addError($msg) {
		$this->errors[] = $msg;
		return true;
	}

	public function save() {
		$dbFields = $this->getFields();

		$fieldsHelper = [];
		$fieldsData = [];

		$class = get_class($this);
		$tableName = $class::$tableName;

		if ($this->exists) {
			$query = 'UPDATE `' . $tableName .'` ' . $this->tableAlias . ' SET ';

			foreach($dbFields as $field) {
				$fieldsHelper[] = ('`' . $this->tableAlias . '`.`' . $field . "`=?");
				$fieldsData[] = $this->{$field};
			}

			$query .= implode(', ', $fieldsHelper);
			$query .= ' WHERE `' . $this->tableAlias . '`.`id`=?';

			$fieldsData[] = $this->id;

		} else {
			$query = 'INSERT INTO `' . $tableName .'` (';

			foreach($dbFields as $field) {
				$fieldsHelper[('`' . $field . '`')] = "?";
				$fieldsData[] = $this->{$field};
			}

			$query .= implode(', ', array_keys($fieldsHelper));
			$query .= ') VALUES (' . implode(', ', array_values($fieldsHelper)) . ')';
		}
		
		DBConnection::query($query, $fieldsData);
		$this->id = DBConnection::lastId();
		
		DBConnection::free();
	}

	/**
	 * @param $queryResource
	 * @return BaseModel[]
	 */
	protected static function getModelsList($queryResource) {
		$ret = [];

		if (!$queryResource) {
			return $ret;
		}

		$class = get_called_class();

		while ($row = $queryResource->fetch_assoc()) {
			$model = new $class();
			$model->fromData($row);
			$ret[] = $model;
		}

		DBConnection::free();

		return $ret;
	}
	
	/**
	 * @param string $where
	 * @param string $order
	 * @return BaseModel[]
	 */
	public static function findAll($where = '', $order = '') {
		$class = get_called_class();
		$tableName = $class::$tableName;
		
		$query = 'SELECT * FROM ' . $tableName;
		if ($where !== '') {
			$query .= ' WHERE ' . $where;
		}
		
		if ($order !== '') {
			$query .= ' ORDER BY ' . $order;
		}
		
		$res = DBConnection::query($query, []);
		return self::getModelsList($res);
	}
	
	/**
	 * @param string $where
	 * @param string $order
	 * @return BaseModel
	 */
	public static function findOne($where = '', $order = '') {
		$class = get_called_class();
		$tableName = $class::$tableName;
		
		$query = 'SELECT * FROM ' . $tableName;
		if ($where !== '') {
			$query .= ' WHERE ' . $where;
		}
		
		if ($order !== '') {
			$query .= ' ORDER BY ' . $order;
		}
		
		$query .= ' LIMIT 1';
		
		$res = DBConnection::query($query, []);
		$data = self::getModelsList($res);
		$item = sizeof($data) > 0 ? $data[0] : null;
		
		if (!is_null($item)) {
			$item->afterFind();
		}
		
		return $item;
	}
	
	/**
	 * @param $id
	 * @return BaseModel
	 */
	public static function findById($id)
	{
		$class = get_called_class();
		$tableName = $class::$tableName;

		/**
		 * @var $model BaseModel
		 */
		$model = new $class();
		$query = "SELECT * FROM `" . $tableName .'` ' . $model->tableAlias . ' WHERE `' . $model->tableAlias . '`.`id`=?';
		$res = DBConnection::query($query, [$id]);
		$data = $res->fetch_assoc();

		if ($data != null) {
			$model->exists = true;
		} else {
			DBConnection::free();
			return null;
		}

		$model->fromData($data);
		DBConnection::free();
		return $model;
	}
} 