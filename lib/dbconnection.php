<?php
/**
 * Created by Pavel Vorobev.
 * Date: 12.05.14
 * Time: 12:46
 */

namespace lib;

class DBConnection
{
    /**
     * @var mysqli
     */
    protected static $dbConnection;
    protected static $dbStatement;

    /**
     * @return mysqli
     */
    public static function getConnection()
    {
        self::__init();
        return self::$dbConnection;
    }

	public static function free() {
		self::$dbStatement->free_result();
	}
	
	public static function escape($string) {
		self::__init();
		return mysqli_real_escape_string(self::$dbConnection, $string);
	}
	
	public static function lastId() {
		self::__init();
		return mysqli_insert_id(self::$dbConnection);
	}

	protected static function __init()
    {
	    $conf = \lib\Application::app()->config;

	    if (!array_key_exists('db', $conf)) {
		    \lib\Application::handleError(0, 'DB Error', 0, 0, 'DB config is not defined.');
	    }

        if (!self::$dbConnection)
            self::$dbConnection = mysqli_connect($conf['db']['host'], $conf['db']['login'], $conf['db']['password'], $conf['db']['db']);

        if (!self::$dbStatement)
            self::$dbStatement = self::$dbConnection->stmt_init();
    }

    private static final function getTypesString($argsArray)
    {
        $typesString = '';

        foreach ($argsArray as $k => $v) {
            switch (gettype($v)) {
                case 'integer':
                    $typesString .= 'i';
                    break;
                case 'double':
                    $typesString .= 'd';
                    break;
                default:
                    if ($v === null || $v == '==BINDATA==') {
                        $typesString .= 'b';
                    } else {
                        $typesString .= 's';
                    }
                    break;
            }
        }

        return $typesString;
    }

    public static function query($query, $data = [], $noExecute = false)
    {
	    self::__init();

        self::$dbStatement->prepare($query);

        $args = array_merge([DBConnection::getTypesString($data)], $data);
        $tmp = [];

        //-- kind of magic
        for ($i = 1; $i < sizeof($args); $i++) {
            $tmp[$i] = $args[$i];
            $args[$i] = &$tmp[$i];
        }

        if (sizeof($data) > 0)
            call_user_func_array([&self::$dbStatement, 'bind_param'], $args);

        if ($noExecute) {
            return self::$dbStatement;
        }

        self::$dbStatement->execute();

        if (sizeof(self::$dbStatement->error_list) > 0) {
	        \lib\Application::handleError(0, 'DB Error', 0, 0, [self::$dbStatement->error_list, debug_backtrace()]);
        }

        unset($tmp);
        unset($args);

        return self::$dbStatement->get_result();
    }
}