<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 19:16
 */

namespace lib;


class Session {

	const NO_KEY = 'APP_SESSION_NO_KEY';

	private $_session;

	public function __construct() {
		session_start();
		$this->_session = &$_SESSION;
	}

	/**
	 * Implementation for default PHP session
	 * @param $name
	 * @param $value
	 */
	protected function _push($name, $value) {
		$this->_session[$name] = $value;
	}

	/**
	 * Implementation for default PHP session
	 * @param $name
	 * @return string
	 */
	protected function _extract($name) {
		if (array_key_exists($name, $this->_session)) {
			return $this->_session[$name];
		} else {
			return Session::NO_KEY;
		}
	}

	public function __get($name) {
		return $this->get($name);
	}

	public function get($name, $default = null) {
		$data = $this->_extract($name);
		return $data === Session::NO_KEY ? null : $data;
	}

	public function __set($name, $value) {
		$this->_push($name, $value);
	}

	public function set($name, $value) {
		$this->_push($name, $value);
	}
} 