<?php
/**
 * Created by Pavel Vorobev.
 * Date: 16.12.14
 * Time: 21:56
 */

namespace lib;

class Application {
	public $config;
	
	/**
	 * @var Session
	 */
	public $session;
	/**
	 * @var Request
	 */
	public $request;

	/**
	 * @var Router
	 */
	public $router;

	/**
	 * @var Application
	 */
	private static $_instance;

	/**
	 * @param $config
	 */
	public function __construct($config) {
		Application::app($this);

		$this->config = $config;
		$this->request = new Request();
		$this->session = new Session();
		$this->router = new Router();
	}

	/**
	 * @param Application $app
	 * @return Application
	 */
	public static function app(&$app = null) {
		if ($app !== null) {
			self::$_instance = $app;
		}

		return self::$_instance;
	}
	
	private function runAction() {
		$info = $this->router->getRoute();
		
		$controller = new $info['class']();
		$methodString = 'action' . ucfirst($info['action']);
		
		if (method_exists($controller, $methodString)) {
			call_user_func([$controller, $methodString]);
		} else {
			\lib\Application::handleError(0, 'Invalid route', 0, 0, 'Action ' . $methodString . ' does not exists.');
		}
	}
	
	public function run() {
		$this->router->parseRoute();
		$this->runAction();
	}
	
	public static function handleError($errno=0, $errstr='', $file='', $line=0, $trace=[]) {
		$renderrer = new BaseController();
		$renderrer->setViewsDir(Application::app()->config['projectRoot'] . '/lib/views/');
		$renderrer->render('error', [
			'errno' => $errno,
			'errstr' => $errstr,
			'file' => $file,
			'line' => $line,
			'trace' => $trace
		]);
	}
}