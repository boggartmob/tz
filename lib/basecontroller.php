<?php
/**
 * Created by Pavel Vorobev.
 * Date: 16.12.14
 * Time: 23:13
 */

namespace lib;


class BaseController {
	
	protected $viewsDir = '';

	public function __construct() {
		$this->viewsDir = Application::app()->config['defaultViewsPath'] . '/';
	}
	
	public function getViewsDir() {
		return $this->viewsDir;
	}

	public function setViewsDir($dir) {
		$this->viewsDir = $dir;
	}
	
	public function parentBegin() {
		ob_start();
		ob_implicit_flush(false);
	}

	public function parentEnd($_viewPath, $_data = []) {
		$content = ob_get_clean();
		if (is_array($_data)) {
			extract($_data);
		} else {
			$data = $_data;
		}
		$viewPath = $this->getViewPath($_viewPath);
		require($viewPath);
	}

	protected function getViewPath($_viewPath) {
		return $this->viewsDir . $_viewPath . '.php';
	}

	public function render($_viewPath, $_data = [], $return = false) {
		$viewPath = $this->getViewPath($_viewPath);

		if (!is_file($viewPath)) {
			trigger_error('Invalid view path ' . $viewPath, E_USER_ERROR);
		}

		if (is_array($_data)) {
			extract($_data);
		} else {
			$data = $_data;
		}

		if ($return) {
			ob_start();
			ob_implicit_flush(false);
			require($viewPath);
			return ob_get_clean();
		} else {
			require($viewPath);
		}
	}


} 