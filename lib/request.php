<?php
/**
 * Created by Pavel Vorobev.
 * Date: 17.12.14
 * Time: 17:00
 */

namespace lib;

/**
 * Simple request class.
 * Can contain input validation and other stuff.
 */
class Request {
	
	const NO_KEY = 'APP_REQUEST_NO_KEY';

	private $_get;
	private $_post;
	private $_files;

	public $emptyFieldNotExists = true;
	public $rawRequest = '';
	
    public function __construct() {
        $this->_get = $_GET ? $_GET : [];;
		$this->_post = $_POST ? $_POST : [];;
		$this->_files = $_FILES ? $_FILES : [];
	    $this->rawRequest = $_SERVER['QUERY_STRING'];
    }
	
	public function __get($name) {
		foreach (['_post', '_get', '_files'] as $field) {
			if (array_key_exists($name, $this->{$field})) {
				return $this->{$field}[$name];
			}
		}
	}

	private function _extract($field, $name) {
		if (array_key_exists($name, $this->{$field})) {
			if ($this->emptyFieldNotExists && $this->{$field}[$name] === '') {
				return Request::NO_KEY;
			} else {
				return $this->{$field}[$name];
			}
		}
		
		return Request::NO_KEY;
	}

	public function post($name, $default = null) {
		$data = $this->_extract('_post', $name);
		return $data == Request::NO_KEY ? $default : $data;
	}
	
	public function get($name, $default = null) {
		$data = $this->_extract('_get', $name);
		return $data == Request::NO_KEY ? $default : $data;
	}
	
	public function file($name, $default = null) {
		$data = $this->_extract('_files', $name);
		return $data == Request::NO_KEY ? $default : $data;
	}
}
