<html>
	<head>
		<meta charset="UTF-8">
		<title>Error</title>
	</head>
	<body>
		<h1>
			<?php print $errstr ? $errstr : ''; ?>
		</h1>
		<p>
			File <?php print $file ? $file : ''; ?>
		</p>
		<p>
			Line <?php print $line ? $line : ''; ?>
		</p>
		<pre><?php $trace ? is_array($trace) ? var_dump($trace) : print $trace : print 'No info.'; ?></pre>
	</body>
</html>
