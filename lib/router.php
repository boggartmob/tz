<?php
/**
 * Created by Pavel Vorobev.
 * Date: 16.12.14
 * Time: 21:56
 */

namespace lib;

class Router {

	public $routeKey = 'r';
	private $route;
	private $routeString = '';

	public function __construct() {
		$routeString = Application::app()->request->get($this->routeKey, false);
		
		if (!$routeString) {
			$routeString = 'index';
		}
		
		$this->routeString = $routeString;
	}

	public function parseRoute() {
		$class = false;

		if (!is_string($this->routeString)) {
			if (!array_key_exists('defaultController', Application::app()->config)) {
				throw new \Exception('Invalid route format');
			} else {
				$class = Application::app()->config['defaultController'];
			}
		}

		$routeData = explode('/', $this->routeString);

		if (sizeof($routeData) < 2) {
			$action = 'index';
		} else {
			$action = array_pop($routeData);
		}

		if (sizeof($routeData) === 1) {
			array_unshift($routeData, 'controllers');
		}

		array_unshift($routeData, '');

		$this->route = [
			'action' => $action,
			'class' => !$class ? implode('\\', $routeData) : $class
		];
	}
	
	public function getRoute() {
		return $this->route;
	}

	protected function makeRouteString($route, $args) {
		$s = '?r=' . $route;

		foreach($args as $argName => $argValue) {
			$s .= '&' . $argName . '=' . $argValue;
		}

		return $s;
	}

	public function redirect($routeData = []) {
		if (empty($routeData)) {
			header('Location: ?' . Application::app()->request->rawRequest);
		} else {
			header('Location: ' . $this->makeRouteString(array_shift($routeData), $routeData));
		}
	}

	public function sendFileToClient($name, $path) {
		if (!is_file($path)) {
			return false;
		}

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . $name);
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($path));
		readfile($path);

		return true;
	}
}