--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.2.280.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 20.12.2014 11:09:40
-- Версия сервера: 5.5.39-MariaDB-1~squeeze-log
-- Версия клиента: 4.1
--


USE tz;

CREATE TABLE IF NOT EXISTS data_type (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  type enum ('string', 'number', 'email') DEFAULT NULL,
  label varchar(255) DEFAULT NULL,
  required tinyint(1) DEFAULT 0,
  PRIMARY KEY (id),
  INDEX type_idx (type)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 5461
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS user (
  id int(11) NOT NULL AUTO_INCREMENT,
  login varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL,
  last_login timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  UNIQUE INDEX login_UNIQUE (login)
)
ENGINE = INNODB
AUTO_INCREMENT = 8
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS file (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id int(11) NOT NULL,
  path varchar(45) DEFAULT NULL,
  removed tinyint(1) DEFAULT 0,
  name varchar(45) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX fk_file_user_idx (user_id),
  CONSTRAINT FK_file_user_id FOREIGN KEY (user_id)
  REFERENCES user (id) ON DELETE RESTRICT ON UPDATE RESTRICT
)
ENGINE = INNODB
AUTO_INCREMENT = 28
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS user_data (
  id int(11) NOT NULL AUTO_INCREMENT,
  data_type_id int(11) NOT NULL,
  data_value varchar(255) DEFAULT NULL,
  date_modified timestamp DEFAULT CURRENT_TIMESTAMP,
  user_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX fk_user_data_data_type_idx (data_type_id),
  INDEX fk_user_data_user_idx (user_id),
  CONSTRAINT FK_user_data_data_type_id FOREIGN KEY (data_type_id)
  REFERENCES data_type (id) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT fk_user_data_user FOREIGN KEY (user_id)
  REFERENCES user (id) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = INNODB
AUTO_INCREMENT = 35
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 6.2.280.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 21.12.2014 19:50:00
-- Версия сервера: 5.6.14
-- Версия клиента: 4.1
--


SET NAMES 'utf8';

INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(1, 'login', 'string', 'Login', 1);
INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(2, 'password', 'string', 'Password', 1);
INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(3, 'fname', 'string', 'First name', 1);
INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(4, 'sname', 'string', 'Second name', 1);
INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(5, 'email', 'email', 'E-mail', 0);
INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(6, 'phone', 'string', 'Phone', 0);
INSERT INTO tz.data_type(id, name, type, label, required) VALUES
(7, 'bdate', 'string', 'Birth date', 0);