<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 22:17
 */

namespace models;


use \lib\Application;
use \lib\BaseModel;
use \lib\DBConnection;

class File extends BaseModel {
	protected static $tableName = 'file';

	public $user_id;
	public $path;
	public $name = '';
	public $removed = 0;

	public function getPath() {
		return Application::app()->config['fileStorePath'] . '/' . $this->path;
	}

	public function saveFile($path, $name, $user_id) {

		if (!is_writable(Application::app()->config['fileStorePath'])) {
			return false;
		}

		$this->name = DBConnection::escape($name);
		$this->path = uniqid('userFile_' . time() . '_');
		$this->user_id = $user_id;
		parent::save();

		return move_uploaded_file($path, $this->getPath());
	}

	public function download() {
		return Application::app()->router->sendFileToClient($this->name, $this->getPath());
	}
}