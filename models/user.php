<?php
/**
 * Created by Pavel Vorobev.
 * Date: 17.12.14
 * Time: 20:57
 */

namespace models;

use \lib\BaseModel;
use \lib\DBConnection;

class User extends BaseModel {
	protected static $tableName = 'user';

	public $login;
	public $password;
	public $last_login;

	/**
	 * @param $login
	 * @return bool
	 */
	public static function userExists($login) {
		return self::findByLogin($login) !== null;
	}

	/**
	 * @param $login
	 * @return User
	 */
	public static function findByLogin($login) {
		$login = DBConnection::escape($login);
		return self::findOne("login='$login'");
	}
}