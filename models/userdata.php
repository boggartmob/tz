<?php
/**
 * Created by Pavel Vorobev.
 * Date: 17.12.14
 * Time: 23:11
 */

namespace models;

use \lib\BaseModel;
use \lib\DBConnection;

class UserData extends BaseModel {
	protected static $tableName = 'user_data';

	public $data_type_id;
	public $data_value;
	public $date_modified;
	public $user_id;

	/**
	 * @param $userId
	 * @return UserData[]
	 */
	public static function getCurrentUserData($userId) {
		$tableName = self::$tableName;
		$userId = intval($userId);

		/**
		 * This SQL can be moved to MySQL view.
		 */
		$sql = <<<SQL
SELECT
  *
FROM $tableName ud1
INNER JOIN (
	SELECT
		ud.user_id AS uid,
		MAX(ud.date_modified) AS dm,
		ud.data_type_id AS dtid
	FROM $tableName ud
	WHERE ud.user_id=? GROUP BY dtid
) subquery
ON subquery.uid = ud1.user_id
AND subquery.dm = ud1.date_modified
AND subquery.dtid = ud1.data_type_id;
SQL;

		$res = DBConnection::query($sql, [$userId]);
		return self::getModelsList($res);
	}
}