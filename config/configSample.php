<?php
/**
 * Created by Pavel Vorobev.
 * Date: 18.12.14
 * Time: 0:02
 */

$projectRoot = __DIR__ . '/..';

return [
	'defaultController' => '\\controllers\\Index',
	'projectRoot' => $projectRoot,
	'fileStorePath' => $projectRoot . '/files',
	'defaultViewsPath' => $projectRoot . '/views',
	'maxUploadSize' => 1024 * 1024,
	'db' => [
		'host' => 'localhost',
		'login' => '',
		'password' => '',
		'db' => 'tz',
	]
];