<?php
/**
 * Created by Pavel Vorobev.
 * Date: 16.12.14
 * Time: 23:12
 */

namespace controllers;

use \lib\Application;
use \lib\BaseController;
use \models\User;
use \models\UserData;
use \models\DataType;


class Index extends BaseController {

	public function actionIndex() {
		$this->render('index/index', ['model' => []]);
	}

	public function actionLogout() {
		Application::app()->session->loggedIn = false;
		Application::app()->session->userId = -1;
		Application::app()->router->redirect(['index/login', 'fromLogoutPage' => 1]);
	}

	public function actionLogin() {
		if (Application::app()->session->loggedIn) {
			$this->render('index/loggedIn');
			return;
		}

		$message = null;
		
		if (Application::app()->request->post('run', false)) {
			$login = Application::app()->request->post('login', '');
			$password = Application::app()->request->post('password', '');

			$user = User::findByLogin($login);

			if (is_null($user) || ($user->password !== md5($password))) {
				$message = 'Invalid login or password.';
			} else {
				Application::app()->session->loggedIn = true;
				Application::app()->session->userId = $user->id;
				$user->last_login = date('Y-m-d H:i:s');
				$user->save();
				$this->render('index/loggedIn');
				return;
			}
		}
		
		$this->render('index/loginForm', ['warningMessage' => $message]);
	}

	public function actionRegister() {
		$dataTypes = DataType::findAll('', 'required DESC');
		$errors = false;
		$dataToStore = [];
		
		if (Application::app()->request->post('run', false)) {
			
			//-- Some other validator can be used instead
			$checkType = function(DataType $field, $postValue) {
				//-- check type here.
				return true;
			};
			
			$noData = '_no_data_' . rand(0, 1000);
			
			foreach ($dataTypes as $k => &$field) {
				
				/**
				 * @var \models\DataType $field
				 */
				
				$data = Application::app()->request->post($field->name, $noData);
				
				if ($data === $noData && $field->required) {
					$errors = $field->addError('Field is required.');
					continue;
				}
				
				if ($data === $noData && !$field->required) {
					$data = '';
				}
				
				if (!$checkType($field, $data)) {
					$errors = $field->addError('Invalid data.');
					continue;
				}
				
				if ($field->name == 'login' && User::userExists($data)) {
					$errors = $field->addError('User with the same login exists. Choose another login please.');
					continue;
				}
				
				$dataModel = new UserData();
				$dataModel->data_type_id = $field->id;
				$dataModel->data_value = $data;
				array_push($dataToStore, $dataModel);
			}
			
			if (!$errors) {
				$user = new User();
				$user->login = Application::app()->request->post('login');
				$user->password = md5(Application::app()->request->post('password'));

				$user->save();
				
				foreach($dataToStore as $dataModel) {
					$dataModel->user_id = $user->id;
					$dataModel->save();
				}
				
				$this->render('index/loginForm', ['message' => 'Registration successful!']);
				return;
			}
		}
		
		$this->render('index/registrationForm', ['dataTypes' => $dataTypes, 'errors' => $errors]);
	}
} 