<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 20:11
 */

namespace controllers;

use \lib\Application;
use \lib\BaseController;
use \lib\DBConnection;
use \models\DataType;
use \models\File;
use \models\UserData;
use \models\User as UserModel;

class User extends BaseController {

	public function __construct() {
		if (!Application::app()->session->loggedIn) {
			Application::app()->router->redirect(['index/login', 'accessDenied' => 1]);
			return;
		}

		parent::__construct();
	}

	public function actionIndex() {
		$this->render('user/index', []);
	}

	public function actionEdit() {
		$dataTypes = DataType::findAll("name<>'login'", 'required DESC');
		$data = UserData::getCurrentUserData(Application::app()->session->userId);

		$keys = [];
		$values = [];
		$editedFields = 0;

		/**
		 * @var $dataTypes DataType[]
		 */
		foreach ($dataTypes as $type) {
			$keys[$type->id] = $type->name;
		}

		foreach ($data as $dataObj) {
			if (!array_key_exists($dataObj->data_type_id, $keys)) {
				//-- Some field types was excluded.
				continue;
			}

			$values[$keys[$dataObj->data_type_id]] = $dataObj->data_value;
		}

		if (Application::app()->request->post('run', false)) {
			foreach ($dataTypes as $k => $type) {
				$data = Application::app()->request->post($type->name, null);

				if ($values[$type->name] === $data) {
					continue;
				}

				$editedFields += 1;

				$dataModel = new UserData();
				$dataModel->data_type_id = $type->id;
				$dataModel->data_value = DBConnection::escape($data);
				$dataModel->user_id = Application::app()->session->userId;
				$dataModel->save();
				$values[$type->name] = $data;

				if ($type->name == 'password') {
					$user = UserModel::findById(Application::app()->session->userId);
					$user->password = md5($data);
					$user->save();
				}
			}

			Application::app()->router->redirect(['user/edit', 'edited' => $editedFields]);
		}

		$this->render('user/edit', [
			'dataTypes' => $dataTypes,
			'errors' => [],
			'values' => $values
		]);
	}

	public function actionFiles() {
		if (Application::app()->request->post('run', false)) {
			$this->uploadFile();
		}

		$files = File::findAll('removed=0 AND user_id=' . Application::app()->session->userId);

		$this->render('user/files', ['files' => $files]);
	}

	public function actionDownload() {
		/**
		 * @var File $file
		 */
		$file = File::findById(Application::app()->request->get('id', 0));

		if ($file && $file->user_id == Application::app()->session->userId) {
			if (!$file->download()) {
				Application::app()->router->redirect(['user/files', 'downloadFaled' => 1]);
			}
		} else {
			Application::app()->router->redirect(['user/files']);
		}
	}

	public function actionDelete() {
		/**
		 * @var File $file
		 */
		$file = File::findById(Application::app()->request->get('id', 0));

		if ($file) {
			if (is_file($file->getPath())) {
				unlink($file->getPath());
			}

			$file->removed = 1;
			$file->save();
 		}

		Application::app()->router->redirect(['user/files']);
	}

	private function uploadFile() {
		$data = Application::app()->request->file('uploadedFile', null);

		if ($data) {
			/**
			 * TODO: Client JS check needed.
			 */
			if ($data['size'] > Application::app()->config['maxUploadSize']) {
				Application::app()->router->redirect(['user/files', 'uploadSuccess' => 0]);
				return;
			}

			$nameData = explode('.', $data['name']);
			$file = new File();
			$success = $file->saveFile(
				$data['tmp_name'],
				Application::app()->request->post('uploadedFileName', 'file') . '.' . array_pop($nameData),
				Application::app()->session->userId
			);

			Application::app()->router->redirect(['user/files', 'uploadSuccess' => $success ? 1 : 0]);
		}
	}
}
