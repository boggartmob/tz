<?php
/**
 * Created by Pavel Vorobev.
 * Date: 16.12.14
 * Time: 21:34
 *
 * PHP 5.5.6 (cli) (built: Nov 12 2013 11:33:44)
 * Copyright (c) 1997-2013 The PHP Group
 * Zend Engine v2.5.0, Copyright (c) 1998-2013 Zend Technologies
 */

set_include_path(
	get_include_path() . PATH_SEPARATOR
	. __DIR__ . '/lib' . PATH_SEPARATOR
	. __DIR__
);

spl_autoload_extensions(".php");
spl_autoload_register();

set_error_handler(['\\lib\\Application', 'handleError'], E_ALL);

$config = require_once(__DIR__ . '/config/main.php');
$app = new \lib\Application($config);
$app->run();