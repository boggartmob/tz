<?php
/**
 * Created by Pavel Vorobev.
 * Date: 16.12.14
 * Time: 23:59
 *
 * @var \controllers\Index $this
 */

$this->parentBegin();

?>

<div class="row">
	<div class="col-lg-12" style="padding-top: 100px;">
		<h1>REGISTRATION FORM</h1>
		<?php
			$this->render('index/_dataForm', ['dataTypes' => $dataTypes, 'errors' => $errors]);
		?>
	</div>
</div>


<?php
$this->render('layouts/_buttons');
$this->parentEnd('layouts/main');