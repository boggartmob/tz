

<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 19:57
 */

$this->parentBegin();

?>

<div class="row" role="form" style="margin-top: 50%;">
	<div class="col-lg-12">
		<div class="alert alert-success">
			LOGIN SUCCESS
		</div>
		<?php
			$this->render('layouts/_buttons');
		?>
	</div>
</div>

<?php
$this->parentEnd('layouts/main');