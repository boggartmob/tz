<div class="form-group <?php print sizeof($errors) > 0 ? 'has-error' : '';?>">
	<label class="control-label" for="<?php print $name; ?>Input"><?php print $label; ?></label>
	<input 
		name="<?php print $name; ?>" 
		value="<?php print $value; ?>"
		type="text" 
		class="form-control" 
		id="<?php print $name; ?>Input" 
		placeholder="Enter <?php print strtolower($label); ?>">
	
	<?php
		foreach($errors as $e) {
			print '<p class="help-block" style="color: #a94442;">' . $e . '</p> ';
		}
	?>
	
</div>