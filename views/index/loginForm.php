<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 19:37
 */

$this->parentBegin();

?>

<div class="row" role="form" style="margin-top: 100px;">
	<div class="col-lg-12">
		<h1>LOGIN FORM</h1>

		<div class="well">

			<?php
			if (isset($message)) {
				print '<span class="label label-success">' . $message . '</span><br/><br/>';
			}
			if (isset($warningMessage)) {
				print '<span class="label label-warning">' . $warningMessage . '</span><br/><br/>';
			}
			?>

			<form method="post" action="?r=index/login">
				<input type="hidden" name="run" value="1"/>
				<div class="form-group">
					<label for="loginInput">Login</label>
					<input
						name="login"
						type="text"
						class="form-control"
						id="loginInput"
						value="<?php print \lib\Application::app()->request->post('login', ''); ?>"
						placeholder="Enter login"/>
				</div>

				<div class="form-group">
					<label for="passwordInput">Password</label>
					<input
						name="password"
						type="password"
						class="form-control"
						id="passwordInput"
						value="<?php print \lib\Application::app()->request->post('password', ''); ?>"
						placeholder="Enter password"/>
				</div>

				<button type="submit" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
</div>

<?php
$this->render('layouts/_buttons');
$this->parentEnd('layouts/main');