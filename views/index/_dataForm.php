<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 20:26
 */
?>

<form role="form" class="well" method="post" action="">
	<input type="hidden" name="run" value="1"/>

	<?php

	$required = null;
	if (!isset($values)) {
		$values = [];
	}

	foreach($dataTypes as $field) {
		/**
		 * @var \models\DataType $field
		 */
		if ($field->required !== $required) {
			$required = $field->required;

			print $required ? '<h3>Required data</h3>' : '<h3>Other data</h3>';
		}

		if (!array_key_exists($field->name, $values)) {
			$value = \lib\Application::app()->request->post($field->name, '');
		} else {
			$value = $values[$field->name];
		}

		$this->render('index/_dataField', [
			'label' => $field->label,
			'name' => $field->name,
			'errors' => $field->errors,
			'value' => $value
		]);
	}

	?>
<button type="submit" class="btn btn-default">Submit</button>
</form>