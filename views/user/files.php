<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 22:16
 */


$this->parentBegin();

?>

	<div class="row" role="form" style="margin-top: 100px;">
		<div class="col-lg-12">
			<h1>User files edit</h1>
			<h3>Files list</h3>
			<?php
				if (\lib\Application::app()->request->get('downloadFaled', false)) {
					print '<span class="label label-danger">Download aborted.</span><br/><br/>';
				}
				if (intval(\lib\Application::app()->request->get('uploadSuccess', -1)) === 0) {
					print '<span class="label label-danger">Upload faled</span><br/><br/>';
				}
				if (intval(\lib\Application::app()->request->get('uploadSuccess', -1)) === 1) {
					print '<span class="label label-success">Upload Success</span><br/><br/>';
				}
			?>

			<table class="table table-hover" role="form">

			<?php
				foreach($files as $file) {
					$this->render('user/_fileItem', ['file' => $file]);
				}
			?>

			</table>

			<hr/>
			<h3>Upload file</h3>
			<form role="form" enctype="multipart/form-data" method="post" action="" class="well">
				<input type="hidden" name="run" value="1"/>
				<div class="form-group">
					<label class="control-label" for="fileInput">File</label>
					<input
						name="uploadedFile"
						type="file"
						class="form-control"
						id="fileInput"/>
				</div>
				<div class="form-group">
					<label class="control-label" for="fileInput">File name</label>
					<input
						name="uploadedFileName"
						type="text"
						class="form-control"
						id="fileInput"/>
				</div>
				<button type="submit" class="btn btn-default">Upload</button>
			</form>
		</div>
	</div>

<?php
$this->render('layouts/_buttons');
$this->parentEnd('layouts/main');