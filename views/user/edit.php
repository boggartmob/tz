<?php
/**
 * Created by Pavel Vorobev.
 * Date: 19.12.14
 * Time: 20:20
 */

$this->parentBegin();

?>

	<div class="row" role="form" style="margin-top: 100px;">
		<div class="col-lg-12">
			<h1>User data edit</h1>

			<?php
				if (\lib\Application::app()->request->get('edited', 0) > 0) {
					print '<span class="label label-success">Data successfully edited.</span><br/><br/>';
				}

				$this->render('index/_dataForm', ['dataTypes' => $dataTypes, 'errors' => $errors, 'values' => $values]);
			?>
		</div>
	</div>

<?php
$this->render('layouts/_buttons');
$this->parentEnd('layouts/main');