<div class="row" role="form">
	<div class="col-lg-12">
		<div class="well">
			<a href="?r=index/login" class="btn btn-primary btn-lg btn-block">Login</a>
			<a href="?r=index/register" class="btn btn-primary btn-lg btn-block">Register</a>

			<?php
				if (\lib\Application::app()->session->loggedIn) {
					print '<a href="?r=user/edit" class="btn btn-default btn-lg btn-block btn-success">Edit user data</a>';
					print '<a href="?r=user/files" class="btn btn-default btn-lg btn-block btn-success">Manage files</a>';
					print '<a href="?r=index/logout" class="btn btn-default btn-lg btn-block btn-warning">Logout</a>';
				}
			?>
		</div>
	</div>
</div>