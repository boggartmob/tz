<html>
	<head>
		<title>
			main
		</title>
		<link rel="stylesheet" href="css/bootstrap.min.css"/>
		<meta charset="UTF-8">
	</head>
	<body>

	<div class="row" style="margin: 0px; padding: 0px;">
		<div class="col-lg-4 col-lg-offset-4">
			<?php
			print $content;
			?>
		</div>
	</div>
	</body>
</html>
